# Guided Installation Script

GIS (Guided Installation Script) helps you through the Arch Linux installation steps. Instead of doing the commands for you it gives information and advices and let you do the command you want.

![](docs/gis.mp4)

You will have all the commands done during the installation process in a separated `.zsh_history` under `/root/arch_install` after using this script. This can be helpful to find out what was done during the installation.


## Installation

Get the script by either cloning this repository and transfering the script to your archiso or directly from th live boot by using:

``` shell
curl -O https://gitlab.com/corentin-pro/gis/-/raw/master/gis
```

Then make it executable:

``` shell
chmod +x gis
```


## Usage

From a live archiso starts the script : `./gis`
